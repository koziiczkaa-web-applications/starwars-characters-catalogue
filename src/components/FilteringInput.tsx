import React from 'react';
import './FilteringInput.scss';

export interface FilteringInputProps {
	onChange: (value: string) => void;
}

export function FilteringInput(props: FilteringInputProps): JSX.Element {
	function onValueChange(e: React.FormEvent<HTMLInputElement>){
		props.onChange(e.currentTarget.value)
	}

	return <div className='filter'>
		<label className='filter-input'>
			<input
				className='filter-input__field'
				id='filter-input'
				type='text'
				placeholder=' '
				onChange={onValueChange}
			/>
			<span className='filter-input__label'>Filter</span>
		</label>
	</div>;
}
