import './App.scss';
import * as React from 'react';
import {CharactersList} from './CharactersList';
import StarWarsLogo from '../resources/images/Star_Wars_Logo.svg';

export function App(): JSX.Element {

	return <div className='main'>
		<StarWarsLogo className='logo'/>
		<div className='characters'>
			<CharactersList/>
		</div>
	</div>;
}

export default App;
