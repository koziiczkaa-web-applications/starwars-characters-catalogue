import NoImage from '../resources/images/no-image.svg';
import {Button} from './Button';
import React from 'react';
import {Character} from '../entities/Character';
import './SingleCharacter.scss';
import Axios, {AxiosResponse} from 'axios';
import {World} from '../entities/World';
import {Film} from '../entities/Film';
import styled, {DefaultTheme, StyledComponent} from 'styled-components';

export interface SingleCharacterProps {
	character: Character;
	id: number;
}

export function SingleCharacter(props: SingleCharacterProps): JSX.Element {
	const [open, setOpen] = React.useState<boolean>(false);
	const [homeworld, setHomeworld] = React.useState<string | undefined>(undefined);
	const [films, setFilms] = React.useState<string[]>([]);
	const [title, setTitle] = React.useState<string>('Show details');

	function onShowDetailsClick(): void {
		setOpen(!open);

		if (!open) {
			setTitle('Hide details');
		} else {
			setTitle('Show details');
		}

		if (!homeworld) {
			Axios.get(props.character.homeworld).then((response: AxiosResponse<World>): void => setHomeworld(response.data.name));
		}

		if (!films.length) {
			Promise.all(props.character.films.map((filmUrl: string) => {
				return Axios.get(filmUrl).then((response: AxiosResponse<Film>): string => response.data.title);
			})).then((results: string[]): void => setFilms(results));
		}
	}

	const CharacterName: StyledComponent<'h3', DefaultTheme> = styled.h3`
      &:before {
        content: '${props.id >= 10 ? (props.id + '.') : ('0' + props.id + '.')}';
      }
	`;

	return <div className='character-tile'>
		<div className='character-tile_left'>
			<NoImage className='character-tile_image'/>
		</div>
		<div className='character-tile_right'>
			<CharacterName className='character-tile_name'>{props.character.name}</CharacterName>
			<div className='character-tile_info'>
				<p className='character-tile_element'>
					Gender:
					<span className='character-tile_element--value'> {props.character.gender}</span>
				</p>
				<p className='character-tile_element'>
					Height:
					<span className='character-tile_element--value'> {props.character.height}</span>
				</p>
				<p className='character-tile_element'>
					Mass:
					<span className='character-tile_element--value'> {props.character.mass}</span>
				</p>
				<p className='character-tile_element'>
					Birth year:
					<span className='character-tile_element--value'> {props.character.birth_year}</span>
				</p>
				{open && homeworld && films.length !== 0 && <>
					<p className='character-tile_element'>Hair color:
						<span className='character-tile_element--value'> {props.character.hair_color}</span>
					</p>
					<p className='character-tile_element'>Skin color:
						<span className='character-tile_element--value'> {props.character.skin_color}</span>
					</p>
					<p className='character-tile_element'>Eye color:
						<span className='character-tile_element--value'> {props.character.eye_color}</span>
					</p>
					<p className='character-tile_element'>Homeworld:
						<span className='character-tile_element--value'> {homeworld}</span>
					</p>
					<p className='character-tile_element'>Appearances:
						<span className='character-tile_element--value'> {films.join(', ')}</span>
					</p>
				</>}
			</div>
			<Button onClick={onShowDetailsClick} title={title}/>
		</div>
	</div>;
}
