import React from 'react';
import './LoadingBar.scss';

export function LoadingBar(): JSX.Element {
	return <div className='loading'>
		<div className='lds-roller'>
			<div/><div/><div/><div/><div/><div/><div/><div/>
		</div>
	</div>
}
