import React from 'react';
import './Button.scss';

export interface ButtonProps{
	onClick: () => void;
	title: string;
}

export function Button(props: ButtonProps): JSX.Element {
	return <>
		<button onClick={props.onClick} className='character-tile_button'>{props.title}</button>
	</>
}
